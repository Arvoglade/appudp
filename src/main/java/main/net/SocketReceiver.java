package main.net;

import jakarta.annotation.PostConstruct;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;
import java.util.stream.Collectors;

@Component
public class SocketReceiver {

    private final SocketData socketData;

    private Thread receiverThread;

    private Map<String, PacketHandler> packetHandlerMap = new HashMap<>();

    public SocketReceiver(SocketData socketData, List<PacketHandler> packetHandlerList) {
        this.socketData = socketData;
        packetHandlerMap.putAll(packetHandlerList.stream().collect(Collectors.toMap(PacketHandler::getName, ch -> ch)));

    }

    @PostConstruct
    public void init() {
        receiverThread = new Thread(() -> startReceiving(getKeyBasedHandlerResolver()));
        receiverThread.start();
    }


    private Consumer<DatagramPacket> getKeyBasedHandlerResolver() {
        return new Consumer<DatagramPacket>() {
            @Override
            public void accept(DatagramPacket datagramPacket) {
                String message = new String(datagramPacket.getData());

                String key;
                int keyIndexEnd = message.indexOf(" ");
                if (keyIndexEnd < 0)
                    key = message;
                else
                    key = message.substring(0, keyIndexEnd);

                PacketHandler handler = packetHandlerMap.get(key);
                if (handler != null)
                    handler.getHandler().accept(datagramPacket);
                else {
                    System.out.print("Unsupported packet command received from " + datagramPacket.getSocketAddress() + " >> " + message);
                }
            }
        };
    }

    private Consumer<DatagramPacket> getDefaultPackageConsumer() {
        return datagramPacket -> System.out.println(datagramPacket.getSocketAddress() + ": " + new String(datagramPacket.getData()));
    }

    private void startReceiving(Consumer<DatagramPacket> packetConsumer) {
        DatagramSocket socket = socketData.getSocket();
        while (true) {
            byte[] buffer = new byte[1024];
            DatagramPacket packet = new DatagramPacket(buffer, buffer.length);

            try {
                socket.receive(packet);
                packet.setData(Arrays.copyOf(packet.getData(), packet.getLength()));
                packetConsumer.accept(packet);
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
    }


}
