package main.net;

import org.springframework.stereotype.Component;

import java.net.InterfaceAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.*;

@Component
public class MachineIpResolver {

    private static Set<String> specialNetPrefix = new HashSet<>(List.of("127", "172", "10", "24"));

    public String resolve() throws SocketException {
        String resultIp = null;
        List<String> listIp = new ArrayList<>();

        Iterator<NetworkInterface> netInterfaces = NetworkInterface.getNetworkInterfaces().asIterator();
        while (netInterfaces.hasNext()) {
            NetworkInterface netInterface = netInterfaces.next();
            List<InterfaceAddress> addressList = netInterface.getInterfaceAddresses();
            for (InterfaceAddress interfaceAddress : addressList) {
                if  (!netInterface.isLoopback()) {
                    String strAddr = interfaceAddress.getAddress().toString().substring(1);
                    if (strAddr.indexOf('.') > 0) {
                        if (notSpecialNet(strAddr)) {
                            resultIp = strAddr;
                            listIp.add(strAddr);
                        }
                    }
                }
            }
        }

        if (resultIp == null) {
            throw new SocketException("Couldn't resolve ip address on this machine: can't fined any address");
        }

        if (listIp.size() > 1) {
            StringBuilder sb = new StringBuilder();
            listIp.forEach(ip -> {sb.append(ip); sb.append("; ");});
            throw new SocketException("Couldn't resolve ip address on this machine: there is more than one address:" + sb.toString());
        }

        return resultIp;
    }

    private static boolean notSpecialNet(String strAddr) {
        String prefix = strAddr.substring(0, strAddr.indexOf('.'));
        return !specialNetPrefix.contains(prefix);
    }
}
