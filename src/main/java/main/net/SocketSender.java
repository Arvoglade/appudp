package main.net;

import org.springframework.stereotype.Component;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetSocketAddress;

@Component
public class SocketSender {

    private final SocketData socketData;

    public SocketSender(SocketData socketData) {
        this.socketData = socketData;
    }

    public void send(String ip, int port, byte[] data) {
        DatagramSocket socket = socketData.getSocket();
        DatagramPacket packet;
        try {
            packet = new DatagramPacket(data, data.length, new InetSocketAddress(ip, port));
            socket.send(packet);
        } catch (IOException e) {
            System.out.println("Packet is not sent. Network is unreachable. Check remote address");
        } catch (IllegalArgumentException e) {
            System.out.println("Remote address is invalid");
        }
    }

    public void send(String ipPort, byte[] data) {
        String ip = SocketData.resolveIpFromIpPort(ipPort);
        int port = SocketData.resolvePortFromIpPort(ipPort);
        send(ip, port, data);
    }

    public void send(byte[] data) {
        send(socketData.getRemoteIp(), socketData.getRemotePort(), data);
    }

}
