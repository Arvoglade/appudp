package main.net;

import java.net.DatagramPacket;
import java.util.function.Consumer;

public interface PacketHandler {

    String getName();

    Consumer<DatagramPacket> getHandler();
}
