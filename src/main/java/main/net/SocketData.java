package main.net;

import jakarta.annotation.PostConstruct;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.net.DatagramSocket;
import java.net.InetSocketAddress;
import java.net.SocketException;

@Component
public class SocketData {

    private DatagramSocket socket;

    private String hostIp;

    @Value("${host.port}")
    private int hostPort = -1;

    @Value("${remote.ip}")
    private String remoteIp;

    @Value("${remote.port}")
    private int remotePort = -1;

    private final MachineIpResolver machineIpResolver;

    // TODO ?
    private String replyIp = null;
    private int replyPort = -1;



    private static Logger log = LoggerFactory.getLogger(SocketData.class);

    public SocketData(MachineIpResolver machineIpResolver) throws SocketException {
        this.machineIpResolver = machineIpResolver;
        this.hostIp = machineIpResolver.resolve();

        log.info("SocketHolder initialized");
    }

    @PostConstruct
    public void initSocket() throws SocketException {

        this.socket = new DatagramSocket(new InetSocketAddress(hostIp, hostPort));

        System.out.println("Socket initialized on : " + hostIp + ":" + hostPort);
        System.out.println("Remote address is : " + remoteIp + ":" + remotePort);
        System.out.println("You can change remote address with command \"sendTo\" command");
    }

    public DatagramSocket getSocket() {
        return socket;
    }

    public String getIp() {
        return hostIp;
    }

    public int getPort() {
        return hostPort;
    }

    public String getRemoteIp() {
        return remoteIp;
    }

    public int getRemotePort() {
        return remotePort;
    }

    public void setRemoteIp(String remoteIp) {
        this.remoteIp = remoteIp;
    }

    public void setRemotePort(int remotePort) {
        this.remotePort = remotePort;
    }

    public String getReplyIp() {
        return replyIp;
    }

    public void setReplyIp(String replyIp) {
        this.replyIp = replyIp;
    }

    public int getReplyPort() {
        return replyPort;
    }

    public void setReplyPort(int replyPort) {
        this.replyPort = replyPort;
    }

    public static String resolveIpFromIpPort(String ipPort) {
        checkIfIpPortFormatValidAndThrow(ipPort);
        return getIp(ipPort);
    }


    public static int resolvePortFromIpPort(String ipPort) {
        checkIfIpPortFormatValidAndThrow(ipPort);
        return getPort(ipPort);
    }

    private static String getIp(String ipPort) {
        String[] s = ipPort.split(":");
        return s[0];
    }

    private static int getPort(String ipPort) {
        String[] s = ipPort.split(":");
        return Integer.parseInt(s[1]);
    }

    public static void checkIfIpPortFormatValidAndThrow(String ipPort) {
        String ip = getIp(ipPort);
        int port = getPort(ipPort);
        InetSocketAddress inetAddress = new InetSocketAddress(ip, port);
    }

    public static boolean checkIfIpPortFormatValid(String ipPort) {
        try {
            checkIfIpPortFormatValidAndThrow(ipPort);
        } catch (IllegalArgumentException e) {
            return false;
        }
        return true;

        //return ipPort.matches("\\d+.\\d+.\\d+.\\d+:\\d+");
    }


}
