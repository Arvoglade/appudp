package main.net.message;

import main.net.PacketHandler;
import main.net.SocketData;
import main.net.SocketSender;
import org.springframework.stereotype.Component;

import java.net.DatagramPacket;
import java.util.function.Consumer;

@Component
public class MessagePacketHandler implements PacketHandler {

    private final SocketSender socketSender;
    private final SocketData socketData;

    private static final String handlerName = "message";

    public MessagePacketHandler(SocketSender socketSender, SocketData socketData) {
        this.socketSender = socketSender;
        this.socketData = socketData;
    }

    @Override
    public String getName() {
        return handlerName;
    }

    @Override
    public Consumer<DatagramPacket> getHandler() {
        return datagramPacket -> {
            socketData.setReplyIp(datagramPacket.getAddress().getHostAddress());
            socketData.setReplyPort(datagramPacket.getPort());

            System.out.println(datagramPacket.getSocketAddress() + " >> "
                    + new String(datagramPacket.getData()).substring(handlerName.length() + 1));
        };
    }
}
