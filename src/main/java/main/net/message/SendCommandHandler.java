package main.net.message;

import main.command.Command;
import main.command.CommandHandler;
import main.net.SocketData;
import main.net.SocketSender;
import org.springframework.stereotype.Component;

@Component
public class SendCommandHandler implements CommandHandler {

    private final SocketData socketData;
    private final SocketSender socketSender;

    // TODO - may be remove application context, and don't use "sendTo" command. Use SocketSender dirrectly,
    //  or create class Sender or MessageSender, which could be used from here and "sendTo"

    public SendCommandHandler(SocketData socketData, SocketSender socketSender) {
        this.socketData = socketData;
        this.socketSender = socketSender;
    }

    @Override
    public String getName() {
        return "send";
    }

    @Override
    public void handleCommand(Command command) {
        if (socketData.getRemoteIp() == null || socketData.getRemotePort() == 0) {
            System.out.println("Receiver is not defined. Use \"sentTo\" command at least one time to save receiver address");
        } else if (command.getLine().trim().length() > getName().length()) {
            StringBuilder text = new StringBuilder("message ");
            text.append(command.getLine().substring(getName().length() + 1));
            socketSender.send(text.toString().getBytes());
        }
    }

    @Override
    public String getManual() {
        return """
                Use "sendTo" to remember remote address, and "send" to send message to this remote address with no need to type it again
                Usage:
                \tsendTo 81.177.136.145:8081 Hello, Mark!
                \tsend What about your sex live?
                Receiver on 81.177.136.145:8081 will see:
                \tHello, Mark!
                \tWhat about your sex live?""";
    }
}
