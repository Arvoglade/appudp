package main.net.message;

import main.command.Command;
import main.command.CommandHandler;
import main.net.SocketData;
import main.net.SocketSender;
import org.springframework.stereotype.Component;

@Component
public class Reply implements CommandHandler {

    private final SocketData socketData;
    private final SocketSender socketSender;

    public Reply(SocketData socketData, SocketSender socketSender) {
        this.socketData = socketData;
        this.socketSender = socketSender;
    }

    @Override
    public String getName() {
        return "reply";
    }

    @Override
    public void handleCommand(Command command) {
        if (socketData.getReplyIp() == null || socketData.getReplyPort() == -1) {
            System.err.println("Reply address is not defined");
        } else if (command.getLine().trim().length() > getName().length()) {
            String text = "message " + command.getLine().substring(getName().length() + 1);
            socketSender.send(socketData.getReplyIp(), socketData.getReplyPort(), text.getBytes());
        }

    }

    @Override
    public String getManual() {
        return "If you got message this command allows you to reply without typing address";
    }
}
