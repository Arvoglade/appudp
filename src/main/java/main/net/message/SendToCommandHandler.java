package main.net.message;

import main.command.Command;
import main.command.CommandHandler;
import main.command.InvalidCommandException;
import main.net.SocketData;
import main.net.SocketSender;
import org.springframework.stereotype.Component;

@Component
public class SendToCommandHandler implements CommandHandler {

    private final SocketData socketData;
    private final SocketSender socketSender;

    public SendToCommandHandler(SocketData socketData, SocketSender socketSender) {
        this.socketData = socketData;
        this.socketSender = socketSender;
    }

    @Override
    public String getName() {
        return "sendTo";
    }

    @Override
    public void handleCommand(Command command) {
        if (command.getParams().size() == 0) {
            throw new InvalidCommandException("missed parameter: ip:port");
        }
        String ipPort = command.get(0);
        try {
            socketData.setRemoteIp(SocketData.resolveIpFromIpPort(ipPort));
            socketData.setRemotePort(SocketData.resolvePortFromIpPort(ipPort));
        } catch (IllegalArgumentException | ArrayIndexOutOfBoundsException e) {
            throw new InvalidCommandException("ip:port is invalid");
        }
        if (command.getParams().size() > 1) {
            StringBuilder text = new StringBuilder("message ");
            int secondSpaceIndex = command.getLine().indexOf(" ", getName().length() + 1);
            text.append(command.getLine().substring(secondSpaceIndex + 1));
            socketSender.send(text.toString().getBytes());
        }
    }

    @Override
    public String getManual() {
        return """
                Use "sendTo" to remember remote address, and send a message to it
                Usage:
                \tsendTo 81.177.136.145:8081 Hello, Mark! What about your sex live?
                Receiver on 81.177.136.145:8081 will see:
                \tHello, Mark! What about your sex live?""";
    }


}
