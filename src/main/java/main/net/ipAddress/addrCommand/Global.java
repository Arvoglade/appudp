package main.net.ipAddress.addrCommand;

import main.command.Command;
import main.net.SocketSender;
import main.net.ipAddress.AddrCommandHandler;
import org.springframework.stereotype.Component;

@Component
public class Global implements AddrCommandHandler {
    private final SocketSender socketSender;

    public Global(SocketSender socketSender) {
        this.socketSender = socketSender;
    }

    @Override
    public String getName() {
        return "global";
    }

    @Override
    public void handleCommand(Command command) {
        socketSender.send("getMyAddress".getBytes());
    }
}
