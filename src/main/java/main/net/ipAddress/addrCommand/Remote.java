package main.net.ipAddress.addrCommand;

import main.command.Command;
import main.net.SocketData;
import main.net.ipAddress.AddrCommandHandler;
import org.springframework.stereotype.Component;

@Component
public class Remote implements AddrCommandHandler {

    private final SocketData socketData;

    public Remote(SocketData socketData) {
        this.socketData = socketData;
    }

    @Override
    public String getName() {
        return "remote";
    }

    @Override
    public void handleCommand(Command command) {
        System.out.println(socketData.getRemoteIp() + ":" + socketData.getRemotePort());
    }
}
