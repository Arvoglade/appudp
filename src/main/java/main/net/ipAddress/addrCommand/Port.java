package main.net.ipAddress.addrCommand;

import main.command.Command;
import main.net.SocketData;
import main.net.ipAddress.AddrCommandHandler;
import org.springframework.stereotype.Component;

@Component
public class Port implements AddrCommandHandler {

    private final SocketData socketData;

    public Port(SocketData socketData) {
        this.socketData = socketData;
    }

    @Override
    public String getName() {
        return "port";
    }

    @Override
    public void handleCommand(Command command) {
        System.out.println(socketData.getPort());
    }
}
