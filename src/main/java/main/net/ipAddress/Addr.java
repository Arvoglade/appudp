package main.net.ipAddress;

import main.command.Command;
import main.command.CommandHandler;
import org.springframework.stereotype.Component;

import java.util.Map;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class Addr implements CommandHandler {

    private static final String commandName = "addr";

    private final Map<String, AddrCommandHandler> addrCommandHandlerMap;
    public Addr(List<AddrCommandHandler> addrCommandHandlerList) {
        this.addrCommandHandlerMap = addrCommandHandlerList.stream().collect(Collectors.toMap(x -> x.getName(), x -> x));
    }

    @Override
    public String getName() {
        return commandName;
    }

    @Override
    public void handleCommand(Command command) {
        findAndHandle(command, 1, addrCommandHandlerMap);
    }

    @Override
    public String getManual() {
        return """
                Use it to get your local ip address (type \"addr local\"), 
                or global address and port (type \"addr global\").
                For last one requires one use of \"sendTo\" command to allow application to remember remote address""";
    }
}
