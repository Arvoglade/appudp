package main.net.ipAddress;

import main.net.PacketHandler;
import main.net.SocketSender;
import org.springframework.stereotype.Component;

import java.net.DatagramPacket;
import java.util.function.Consumer;

@Component
public class GetMyAddressPacketHandler implements PacketHandler {

    private static final String handlerName = "getMyAddress";

    private final SocketSender socketSender;

    public GetMyAddressPacketHandler(SocketSender socketSender) {
        this.socketSender = socketSender;
    }

    @Override
    public String getName() {
        return handlerName;
    }

    @Override
    public Consumer<DatagramPacket> getHandler() {
        return datagramPacket -> {
            String[] words = new String(datagramPacket.getData()).split(" ");
            if (words.length == 1) {
                String ip = datagramPacket.getAddress().getHostAddress();
                int port = datagramPacket.getPort();
                String message = "getMyAddress result " + ip + ":" + port;
                socketSender.send(ip, port, message.getBytes());
            } else if (words[1].equals("result") && words.length >= 3) {
                System.out.println("Your global (white) address is " + words[2]);
            }
        };
    }
}
