package main.net.pingPong;

import main.net.PacketHandler;
import main.net.SocketSender;
import org.springframework.stereotype.Component;

import java.net.DatagramPacket;
import java.util.function.Consumer;

@Component
public class PingPongPacketHandler implements PacketHandler {

    private final PingPongWorkerManager pingPongWorkerManager;

    public PingPongPacketHandler(PingPongWorkerManager pingPongWorkerManager) {
        this.pingPongWorkerManager = pingPongWorkerManager;
    }

    @Override
    public String getName() {
        return "ping-pong";
    }

    @Override
    public Consumer<DatagramPacket> getHandler() {
        return (packet) -> {
            String text = new String(packet.getData());
            String ip = packet.getAddress().getHostAddress();
            int port = packet.getPort();

            String[] commands = text.split(" ");
            if (commands[1].equals("pong")) {
                if (commands.length >= 3 && commands[2].equals("start")) {
                    int timeToWait = 10000;
                    try {
                        timeToWait = Integer.parseInt(commands[3]);
                    } catch (Exception e) {

                    }
                    pingPongWorkerManager.startNewWorker(ip, port, timeToWait);
                } else if (commands.length >= 3 && commands[2].equals("stop")) {
                    pingPongWorkerManager.stopWorker(ip + ":" + port);
                }
            } else if (commands[1].equals("ping")) {
                //System.out.println("ping from " + packet.getAddress().getHostAddress() + ":" + packet.getPort());
                pingPongWorkerManager.setTimeOfLastIncomingPing(System.currentTimeMillis());
            }
        };
    }
}
