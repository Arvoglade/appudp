package main.net.pingPong;

import main.command.Command;
import main.command.InvalidCommandException;
import main.command.CommandHandler;
import org.springframework.stereotype.Component;

import java.util.*;
import java.util.stream.Collectors;

@Component
public class PingPong implements CommandHandler {

    private static final Set<String> keys = new HashSet<>(List.of("-t"));

    private final Map<String, PingPongCommandHandler> pingPongCommandHandlerMap;

    public PingPong(List<PingPongCommandHandler> pingPongCommandHandlerList) {
        this.pingPongCommandHandlerMap = pingPongCommandHandlerList.stream().collect(Collectors.toMap(x -> x.getName(), x -> x));
    }

    @Override
    public String getName() {
        return "ping-pong";
    }

    @Override
    public void handleCommand(Command command) {
        Command commandKeys = new Command(command.getLine(), keys);
        findAndHandle(commandKeys, 1, pingPongCommandHandlerMap);
    }

    @Override
    public String getManual() {
        return "Command to establish and support connection for a long period of time. And there I should be written some use cases, but I ain't";
    }


}
