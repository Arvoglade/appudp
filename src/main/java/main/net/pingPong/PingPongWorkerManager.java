package main.net.pingPong;

import main.command.InvalidCommandException;
import main.net.SocketData;
import main.net.SocketSender;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

@Component
public class PingPongWorkerManager {
    private final SocketSender socketSender;
    private final SocketData socketData;

    private final Map<String, PingPongWorker> workers = new HashMap<>();

    private String incomingPong = null;
    private long timeOfLastIncomingPing = -1;
    int workerNextId = 0;

    public PingPongWorkerManager(SocketSender socketSender, SocketData socketData) {
        this.socketSender = socketSender;
        this.socketData = socketData;
    }

    /*
    TODO
        - нужен ThreadPoolExecutor. Вспомнить как управлять им (в идеале)
        - как мне тут принимать пакеты? Отдельный класс сделать?
     */

    public void startNewWorker(int timeInMillisecondsToSleep) {
        checkRemoteAddress();
        startNewWorker(socketData.getRemoteIp(), socketData.getRemotePort(), timeInMillisecondsToSleep);
    }

    public void startNewWorker(String ip, int port, int timeInMillisecondsToSleep) {
        PingPongWorker pingPongWorker = new PingPongWorker(socketSender, ip, port, timeInMillisecondsToSleep);
        workers.put(ip + ":" + port, pingPongWorker);
        pingPongWorker.start();
        System.out.println("pong-pong to " + ip + ":" + port + " started with " + timeInMillisecondsToSleep + "ms period");
    }

    public void requestPong(int timeToWait) {
        System.out.println("Pong requested");
        String message = "ping-pong pong start " + timeToWait;
        String ip = socketData.getRemoteIp();
        int port = socketData.getRemotePort();
        incomingPong = ip + ":" + port;
        socketSender.send(ip , port, message.getBytes());
        // TODO Возможно реализовать: подождать первого ответного пакета несколько секунд и оповестить о результате
    }

    public void stopWorker(String ipPort) {
        PingPongWorker worker  = workers.get(ipPort);
        if (worker != null) {
            worker.interrupt();
        }
    }

    public String getIncomingPongIpPort() {
        return incomingPong;
    }

    private void checkRemoteAddress() {
        if (socketData.getRemoteIp() == null || socketData.getRemotePort() == -1)
            throw new InvalidCommandException("Remote address is not defined");
    }

    public void stopAllWorkers() {
        workers.forEach((id, worker) -> {worker.interrupt();} );
    }

    public long getTimeOfLastIncomingPing() {
        return timeOfLastIncomingPing;
    }

    public void setTimeOfLastIncomingPing(long timeOfLastIncomingPing) {
        this.timeOfLastIncomingPing = timeOfLastIncomingPing;
    }
}
