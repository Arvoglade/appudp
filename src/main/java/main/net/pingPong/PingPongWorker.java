package main.net.pingPong;

import main.net.SocketSender;

public class PingPongWorker extends Thread {

    private final SocketSender socketSender;

    String remoteIp;
    int remotePort;
    int timeInMillisecondsToSleep;

    public PingPongWorker(SocketSender socketSender, String ip, int port, int timeInMillisecondsToSleep) {
        this.socketSender = socketSender;
        this.remoteIp = ip;
        this.remotePort = port;
        this.timeInMillisecondsToSleep = timeInMillisecondsToSleep;
    }


    @Override
    public void run() {
        while (true) {
            sendPing();
            try {
                Thread.sleep(timeInMillisecondsToSleep);
            } catch (InterruptedException e) {
                break;
            }
        }
        System.out.println("ping-pong stopped");
    }

    private int val = 0;
    private void sendPing() {
        socketSender.send(remoteIp, remotePort, ("ping-pong ping " + val++).getBytes());
    }

}
