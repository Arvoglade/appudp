package main.net.pingPong.pingPongCommand;

import main.command.Command;
import main.net.SocketSender;
import main.net.pingPong.PingPongCommandHandler;
import main.net.pingPong.PingPongWorkerManager;
import org.springframework.stereotype.Component;

@Component
public class Stop implements PingPongCommandHandler {

    private final PingPongWorkerManager pingPongWorkerManager;
    private final SocketSender socketSender;

    public Stop(PingPongWorkerManager pingPongWorkerManager, SocketSender socketSender) {
        this.pingPongWorkerManager = pingPongWorkerManager;
        this.socketSender = socketSender;
    }

    @Override
    public String getName() {
        return "stop";
    }

    @Override
    public void handleCommand(Command command) {
        pingPongWorkerManager.stopAllWorkers();

        String ipPort = pingPongWorkerManager.getIncomingPongIpPort();
        if (ipPort != null) {
            String text = "ping-pong pong stop";
            socketSender.send(ipPort, text.getBytes());
            System.out.println("pong stopped");
        }
    }
}
