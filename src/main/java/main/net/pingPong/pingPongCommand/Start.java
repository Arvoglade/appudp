package main.net.pingPong.pingPongCommand;

import main.command.Command;
import main.command.InvalidCommandException;
import main.net.pingPong.PingPongCommandHandler;
import main.net.pingPong.PingPongWorkerManager;
import org.springframework.stereotype.Component;

@Component
public class Start implements PingPongCommandHandler {

    private final PingPongWorkerManager pingPongWorkerManager;

    public Start(PingPongWorkerManager pingPongWorkerManager) {
        this.pingPongWorkerManager = pingPongWorkerManager;
    }

    @Override
    public String getName() {
        return "start";
    }

    @Override
    public void handleCommand(Command command) {
        int timeToWait = 10000;
        if (command.getKeys().containsKey("-t")) {
            try {
                timeToWait = Integer.parseInt(command.getKeys().get("-t"));
            } catch (NumberFormatException e) {
                throw new InvalidCommandException("after -t should be a number - time in milliseconds");
            }
        }
        if (command.getParams().size() > 1) {
            if (command.get(1).equals("pong")) {
                pingPongWorkerManager.requestPong(timeToWait);
            } else {
                System.err.println("\"" + command.get(1) + "\" is not an option");
            }
        } else {
            pingPongWorkerManager.startNewWorker(timeToWait);
        }


    }
}
