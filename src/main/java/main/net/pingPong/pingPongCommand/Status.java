package main.net.pingPong.pingPongCommand;

import main.command.Command;
import main.net.pingPong.PingPongCommandHandler;
import main.net.pingPong.PingPongWorkerManager;
import org.springframework.stereotype.Component;

@Component
public class Status implements PingPongCommandHandler {

    private final PingPongWorkerManager pingPongWorkerManager;

    public Status(PingPongWorkerManager pingPongWorkerManager) {
        this.pingPongWorkerManager = pingPongWorkerManager;
    }

    @Override
    public String getName() {
        return "status";
    }

    @Override
    public void handleCommand(Command command) {
        long time = pingPongWorkerManager.getTimeOfLastIncomingPing();
        if (time <= 0) {
            System.out.println("ping pong has not started yet");
        } else {
            int timeSinceLastPingInSeconds = (int) (System.currentTimeMillis() - time) / 1000;
            System.out.println(timeSinceLastPingInSeconds + "s since last ping");
        }
    }
}
