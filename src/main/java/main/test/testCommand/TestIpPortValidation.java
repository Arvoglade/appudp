package main.test.testCommand;

import main.command.Command;
import main.command.InvalidCommandException;
import main.net.SocketData;
import main.test.TestCommandHandler;
import org.springframework.stereotype.Component;

@Component
public class TestIpPortValidation implements TestCommandHandler {
    @Override
    public String getName() {
        return "valid";
    }

    @Override
    public void handleCommand(Command command) {
        if (command.getParams().size() < 2) {
            throw new InvalidCommandException("Must contain third argument - ip:port in string representation");
        } else {
            System.out.println("Valid?: " + SocketData.checkIfIpPortFormatValid(command.get(1)));
            try {
                SocketData.checkIfIpPortFormatValidAndThrow(command.get(1));
            } catch (IllegalArgumentException e) {
                System.err.println(e.getMessage());
            }
        }
    }
}
