package main.test.testCommand;

import main.command.Command;
import main.test.TestCommandHandler;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

@Component
public class TestSome implements TestCommandHandler {

    private final ApplicationContext context;

    public TestSome(ApplicationContext context) {
        this.context = context;
    }

    @Override
    public String getName() {
        return "some";
    }

    @Override
    public void handleCommand(Command command) {

    }
}
