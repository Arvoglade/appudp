package main.test.testCommand;

import main.command.Command;
import main.command.InvalidCommandException;
import main.test.TestCommandHandler;
import org.springframework.stereotype.Component;

import java.util.HashSet;
import java.util.List;
import java.util.Scanner;
import java.util.Set;

@Component
public class TestCommandKeys implements TestCommandHandler {

    private static final Set<String> keys = new HashSet<>(List.of("-a", "-b", "--double", "-once"));
    private static final Set<String> flags = new HashSet<>(List.of("-b", "-c"));

    @Override
    public String getName() {
        return "command-keys";
    }

    @Override
    public void handleCommand(Command command) {

        while (true) {
            try {
                System.out.println("Type your command to test or \"q\" to exit test key:");
                command = new Command(new Scanner(System.in).nextLine());
                if (command.getName().equals("q"))
                    break;
                command = new Command(command.getLine(), keys, flags);
                System.out.println(parseCommandKeysToString(command));

            } catch (InvalidCommandException e) {
                System.err.println(e.getMessage());
            }
        }
    }

    public String getManual() {
        return """
                Allow to test command parsing: there are keys: -a, -b; flags: -b, -c.
                You can type your command and see how application parse it""";
    }

    public static String parseCommandKeysToString(Command commandKeys) {
        StringBuilder sb = new StringBuilder();
        sb.append("Name: " + commandKeys.getName());
        sb.append("\nKeys: ");
        commandKeys.getKeys().entrySet().stream().forEach(e -> {
            sb.append(e.getKey());
            sb.append(" ");
            sb.append(e.getValue());
            sb.append(" ");
        });
        sb.append("\nFlags: ");
        commandKeys.getFlags().stream().forEach(e -> {
            sb.append(e);
            sb.append(" ");
        });
        sb.append("\nParams: ");
        commandKeys.getParams().stream().forEach(e -> {
            sb.append(e);
            sb.append(" ");
        });
        return sb.toString().trim();
    }
}
