package main.test;

import main.command.Command;
import main.command.CommandHandler;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Component
public class Test implements CommandHandler {

    private final Map<String, TestCommandHandler> testCommandHandlerMap;

    public Test(List<TestCommandHandler> testCommandHandlerList) {
        this.testCommandHandlerMap = testCommandHandlerList.stream().collect(Collectors.toMap(ch -> ch.getName(), ch -> ch));
    }


    @Override
    public String getName() {
        return "test";
    }

    @Override
    public void handleCommand(Command command) {
        if (command.getParams().size() == 0) {
            System.out.println(getManual());
        } else if (command.get(0).equals("list")) {
            System.out.println("List of test-commands: ");
            for (String testCommandName : testCommandHandlerMap.keySet()) {
                System.out.println("\t" + testCommandName);
            }
        } else {
            findAndHandle(command, 1, testCommandHandlerMap);
        }
    }

    @Override
    public String getManual() {
        return """
                For testing some commands. Type \"test <command name>\"  to make test. Type \"test list\" to list all test-commands""";
    }

}
