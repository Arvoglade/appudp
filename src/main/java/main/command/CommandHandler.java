package main.command;

public interface CommandHandler extends BaseCommandHandler {

    String getName();

    void handleCommand(Command command);

    String getManual();

}
