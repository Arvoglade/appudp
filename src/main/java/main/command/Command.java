package main.command;

import java.util.*;

public class Command {

    private String line;
    private String name;
    private List<String> params;
    private Map<String, String> keys;
    private Set<String> flags;
    private boolean dashPrefixAlwaysKeyOrFlag = true;



    public String getName() {
        return name;
    }

    public String getLine() {
        return line;
    }

    public String get(int index) {
        return params.get(index);
    }

    public List<String> getParams() {
        return params;
    }

    public Map<String, String> getKeys() {
        return keys;
    }

    public Set<String> getFlags() {
        return flags;
    }

    public Command(String line) throws InvalidCommandException {
        this(line, new HashSet<>(), new HashSet<>(), false);
    }

    public Command(String line, boolean dashPrefixAlwaysKeyOrFlag) throws InvalidCommandException {
        this(line, new HashSet<>(), new HashSet<>(), dashPrefixAlwaysKeyOrFlag);
    }

    public Command(String line, Set<String> valueKeys) throws InvalidCommandException {
        this(line, valueKeys, new HashSet<>(), true);
    }

    public Command(String line, Set<String> valueKeys, Set<String> flagKeys) throws InvalidCommandException {
        this(line, valueKeys, flagKeys, true);
    }

    public Command(String line, Set<String> valueKeys, Set<String> flagKeys, boolean dashPrefixAlwaysKeyOrFlag) throws InvalidCommandException {
        this.line = line.replaceAll("\\s+", " ");
        this.dashPrefixAlwaysKeyOrFlag = dashPrefixAlwaysKeyOrFlag;
        this.keys = new HashMap<>();
        this.flags = new HashSet<>();
        this.params = new ArrayList<>();
        initKeysAndParams(this.line.split(" "), valueKeys, flagKeys);
    }

    private void initKeysAndParams(String[] words, Set<String> keys, Set<String> flags) throws InvalidCommandException {
        name = words[0];

        String temp = null;
        OUTER:
        for (int i = words.length - 1; i > 0; i--) {
            if (temp != null) {
                for (String key : keys) {
                    if (words[i].startsWith(key)) {
                        if (words[i].length() == key.length()) {
                            this.keys.put(key, temp);
                            temp = null;
                            continue OUTER;
                        } else {
                            this.params.add(temp);
                            temp = null;
                            this.keys.put(key, words[i].substring(key.length()));
                            continue OUTER;
                        }
                    }
                }
                for (String flag : flags) {
                    if (words[i].startsWith(flag)) {
                        if (words[i].length() == flag.length()) {
                            this.params.add(temp);
                            temp = null;
                            this.flags.add(flag);
                            continue OUTER;
                        } else {
                            this.params.add(temp);
                            temp = null;
                            this.params.add(words[i].substring(flag.length()));
                            this.flags.add(flag);
                            continue OUTER;
                        }
                    }
                }
            } else {
                for (String key : keys) {
                    if (words[i].startsWith(key)) {
                        if (words[i].length() == key.length()) {
                            if (!flags.contains(key))
                                throw new InvalidCommandException("after key \"" + key + "\" should be a value");
                        } else {
                            this.keys.put(key, words[i].substring(key.length()));
                            continue OUTER;
                        }
                    }
                }
                for (String flag : flags) {
                    if (words[i].startsWith(flag)) {
                        if (words[i].length() == flag.length()) {
                            this.flags.add(flag);
                            continue OUTER;
                        } else {
                            this.flags.add(flag);
                            params.add(words[i].substring(flag.length()));
                            continue OUTER;
                        }
                    }
                }

            }

            if (dashPrefixAlwaysKeyOrFlag) {
                if (words[i].startsWith("-")) {
                    throw new InvalidCommandException("Invalid key or flag \"" + words[i].substring(0,2) + "\"");
                }
            }

            if (temp != null) {
                params.add(temp);
                temp = null;
            }

            temp = words[i];

        }
        if (temp != null)
            params.add(temp);

        List<String> tempParams = new ArrayList<>();
        for (int i = params.size() - 1; i >= 0; i--) {
            tempParams.add(params.get(i));
        }
        params = tempParams;

    }


}
