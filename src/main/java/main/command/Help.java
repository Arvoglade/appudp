package main.command;

import main.Runner;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

import java.util.Map;
import java.util.TreeMap;

@Component
public class Help implements CommandHandler {

    private final ApplicationContext applicationContext;

    public Help(ApplicationContext applicationContext) {
        this.applicationContext = applicationContext;
    }


    @Override
    public String getName() {
        return "help";
    }

    @Override
    public void handleCommand(Command command) {

        Runner runner = applicationContext.getBean(Runner.class);
        if (command.getParams().size() == 0) {

            System.out.println("=== HELP ===");

            Map<String, CommandHandler> sortedMap = new TreeMap<>(runner.commandHandlerMap);
            for (Map.Entry<String, CommandHandler> entry : sortedMap.entrySet()) {
                System.out.println(">>> " + entry.getKey());
                System.out.println(entry.getValue().getManual());
                System.out.println();
            }
        } else if (command.getParams().size() == 1) {
            System.out.println(runner.commandHandlerMap.get(command.get(0)).getManual());
        }
    }

    @Override
    public String getManual() {
        return "Type \"help\" to get all commands list with description. Type \"help <commandName>\" to get description of this command";
    }
}
