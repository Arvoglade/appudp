package main.command;

import org.springframework.stereotype.Component;

@Component
public class Exit implements CommandHandler {
    @Override
    public String getName() {
        return "exit";
    }

    @Override
    public void handleCommand(Command command) {
        System.exit(0);
    }

    @Override
    public String getManual() {
        return "Exiting the program";
    }
}
