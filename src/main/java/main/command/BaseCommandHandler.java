package main.command;

import java.util.Map;

public interface BaseCommandHandler {
    String getName();

    void handleCommand(Command command);

    default void findAndHandle(Command command, int position, Map<String, ? extends BaseCommandHandler> handlerMap) {
        String commandName;
        if (position == 0)
            commandName = command.getName();
        else {
            if (command.getParams().size() < position)
                throw new InvalidCommandException("command should have more parameters");
            commandName = command.get(position - 1);
        }
        BaseCommandHandler handler = handlerMap.get(commandName);
        if (handler == null) {
            System.err.println("\"" + commandName + "\" not an option");
            return;
        }
        handler.handleCommand(command);
    }

}
