package main;

import main.command.Command;
import main.command.CommandHandler;
import main.command.InvalidCommandException;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.stream.Collectors;

@Component
public class Runner implements CommandLineRunner {
    public Map<String, CommandHandler> commandHandlerMap = new HashMap<>();

    public Runner(List<CommandHandler> commandHandlerList) {
        commandHandlerMap.putAll(commandHandlerList.stream().collect(Collectors.toMap(CommandHandler::getName, ch -> ch)));
    }

    public void run(String[] args) throws IOException {
        runConsoleInterface();
    }

    private void runConsoleInterface() {
        System.out.println("Ready to accept your commands");
        Scanner scanner = new Scanner(System.in);
        while (true) {
            String line = scanner.nextLine();
            if (line.isBlank() || line.isEmpty())
                continue;
            Command command = new Command(line);
            CommandHandler commandHandler = commandHandlerMap.get(command.getName());
            if (commandHandler == null) {
                System.out.println("\"" + command.getName() + "\"" + " not a command");
                continue;
            }
            try {
                commandHandler.handleCommand(command);
            } catch (InvalidCommandException e) {
                System.err.println(e.getMessage());
            } catch (Exception e) {
                System.err.println("Command have thrown an exception");
                e.printStackTrace();
            }
        }
    }
}
