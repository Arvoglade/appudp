# AppUDP

Application allows to send messges from first machine with not known global ip adress, to other machine with known white global ip adress, and send messages back from server to first machine, while provider's NAT table persist ip and port of first machine.

Allow to get temporal global ip adress which have been assigned by provider for this request.

For these abilities there has to be one another instance of this application which is running on server with global ip adress.
